import plotly.express as px


def plot_cities(us_cities):
    fig = px.scatter_mapbox(
        us_cities,
        lat="lat",
        lon="lon",
        hover_name="City",
        hover_data=["State", "Population"],
        color_discrete_sequence=["fuchsia"],
        zoom=3,
        height=300,
        width=500
    )
    fig.update_layout(mapbox_style="open-street-map")
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
    return fig
